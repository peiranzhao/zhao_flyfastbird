﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBackground : MonoBehaviour
{
    // Start is called before the first frame update
    public float backgroundSpeed;
    public Renderer backgroundRenderer;
    public float time = 5;
    public float addspeed ;
  
    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time >= 10)
        {
            time = 0;
            backgroundSpeed += addspeed;
        }
        backgroundRenderer.material.mainTextureOffset += new Vector2(backgroundSpeed * Time.deltaTime, 0f);
    }
}
