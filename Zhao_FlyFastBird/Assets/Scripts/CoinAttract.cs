﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAttract : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject _myPlayer;
    public static bool magnetActive = false;
    void Start()
    {
        _myPlayer = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(magnetActive)
        {
            MoveTowardsPlayer();
        }
        
    }

    void MoveTowardsPlayer()
    {
        transform.position = Vector3.Lerp(this.transform.position, _myPlayer.transform.position, 3f * Time.deltaTime);
    }
}
